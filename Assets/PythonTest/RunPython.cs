﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics; // Process
using System.IO; // StreamWriter
using System.Threading;

using System.Net;
using System.Net.Sockets;

public class RunPython : MonoBehaviour {

	// Use this for initialization
	void Start () {
		// the python program as a string. Note '@' which allow us to have a multiline string
		
		Process p = new Process(); // create process (i.e., the python program
		p.StartInfo.FileName = "python.exe";
		p.StartInfo.RedirectStandardOutput = false;
		p.StartInfo.UseShellExecute = false; // make sure we can read the output from stdout
		p.StartInfo.Arguments = "C:\\Users\\n0248939\\Documents\\actionrecognition\\IPC.py"; // start the python program with two parameters
		p.Start(); // start the process (the python program)
		//	StreamReader s = p.StandardOutput; 
		//String output = s.ReadToEnd();
		//string []r = output.Split(new char[]{' '}); // get the parameter
		//Console.WriteLine(output);
		//Console.WriteLine(r[0]);


		TcpClient client = new TcpClient();
		
		IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 5007);
		
		client.Connect(serverEndPoint);
		
		NetworkStream clientStream = client.GetStream();
		
		ASCIIEncoding encoder = new ASCIIEncoding();
		byte[] buffer = encoder.GetBytes("Hello Server!");
		
		clientStream.Write(buffer, 0 , buffer.Length);
		clientStream.Flush ();

		Console.WriteLine("Disconnecting from server...");

		byte[] data = new byte[1024];
		int recv;
		String stringData;
		int counter = 1;
		while (true) {
			String input = "Sent from client " + counter.ToString();
			if (input != null){
				clientStream.Write(Encoding.ASCII.GetBytes(input), 0, input.Length);
				clientStream.Flush();
			}
			
			data = new byte[1024];
			recv = clientStream.Read(data, 0, data.Length);
			stringData = Encoding.ASCII.GetString(data, 0, recv);
			print(stringData);
			Thread.Sleep(5000);
			counter++;
		}
		
		clientStream.Close();
		client.Close();
		
		p.WaitForExit();
		//Console.ReadLine(); // wait for a key press
	}	
	
	// Update is called once per frame
	void Update () {
	
	}
}
