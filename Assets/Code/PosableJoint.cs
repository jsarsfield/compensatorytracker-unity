﻿using UnityEngine;
using System.Collections;
using System;
using Windows.Kinect;


[Serializable]
public class PosableJoint
{
    [SerializeField] private string      _bone_name;
    [SerializeField] private string      _target_name;
    [SerializeField] private Vector3     _look_up;
    [SerializeField] private int         _kinect_joint_index;

    [SerializeField] private bool        _editor_tab_open;
    [SerializeField] private bool        _editor_display_gizmo;
    [SerializeField] private bool        _posing_enabled;

    [SerializeField] private GameObject  _bone;
    [SerializeField] private GameObject  _target;

    [SerializeField] private GameObject  _helper_object;
    [SerializeField] private Quaternion  _rotation_offset;


    public string bone_name             { get { return _bone_name; } }
    public string target_name           { get { return _target_name; } }
    public Vector3 look_up              { get { return _look_up; } }
    public int kinect_joint_index       { get { return _kinect_joint_index; } }

    public bool editor_tab_open         { get { return _editor_tab_open; }          set { _editor_tab_open = value; } }
    public bool editor_display_gizmo    { get { return _editor_display_gizmo; }     set { _editor_display_gizmo = value; } }
    public bool posing_enabled          { get { return _posing_enabled; }           set { _posing_enabled = value; } }

    public GameObject bone              { get { return _bone; }                     set { _bone = value; } }
    public GameObject target            { get { return _target; }                   set { _target = value; } }

    public GameObject helper_object     { get { return _helper_object; } }
    public Quaternion rotation_offset   { get { return _rotation_offset; }          set { _rotation_offset = value; } }


    public bool has_bone                { get { return _bone != null; } }
    public bool has_bone_and_target     { get { return has_bone && _target != null; } }
    public Vector3 direction            { get { return target.transform.position - bone.transform.position; } }
    public bool has_valid_direction     { get { return has_bone_and_target && direction != Vector3.zero; } }


    public PosableJoint(int kinect_joint_index, string bone_name, string target_name, Vector3 target_up)
    {
        this._kinect_joint_index = kinect_joint_index;
        this._bone_name = bone_name;
        this._target_name = target_name;
        this._look_up = target_up;

        this._editor_tab_open = false;
        this._editor_display_gizmo = true;
        this._posing_enabled = false;

        this._bone = null;
        this._target = null;

        this._helper_object = null;
        this._rotation_offset = Quaternion.identity;
    }

    public void AutoComputeRotation()
    {
        if (has_valid_direction)
        {
            _rotation_offset = Quaternion.LookRotation(_target.transform.position - _bone.transform.position, _look_up) * 
                Quaternion.AngleAxis(90, new Vector3(1, 0, 0));
        }
    }

    public void CreateHelperBone()
    {
        if (has_bone && _editor_display_gizmo)
        {
            _helper_object = new GameObject("OBJ_" + _bone_name);
            _helper_object.transform.position = _bone.transform.position;
            _helper_object.transform.rotation = _rotation_offset;
            _helper_object.transform.parent = _bone.transform.parent;
            _bone.transform.parent = _helper_object.transform;
        }

        _look_up = Vector3.zero;
        _editor_tab_open = false;
        _editor_display_gizmo = false;
        _bone = null;
        _target = null;
        _rotation_offset = Quaternion.identity;
    }

    public void PoseJoint(Body body_pose)
    {
        if (_helper_object != null && _posing_enabled)
        {
            // TODO: Account for model rotation, so we can move rig around without it borking
            //Vector3 angles = body_pose.Joints[_kinect_joint_index].Orientation.eulerAngles;

            // pull this out the the stock kinect sdk
            Windows.Kinect.Vector4 kinect_orientation = body_pose.JointOrientations[(JointType)_kinect_joint_index].Orientation;   
            Quaternion kinect_quaternion = new Quaternion(kinect_orientation.X, kinect_orientation.Y, kinect_orientation.Z, kinect_orientation.W);
            Vector3 angles = kinect_quaternion.eulerAngles;

            angles.x = -angles.x;   
            angles.z = -angles.z;
            Quaternion rotation = Quaternion.identity;
            rotation.eulerAngles = angles;
            //_helper_object.transform.rotation = rotation;

            _helper_object.transform.rotation = Quaternion.Lerp(_helper_object.transform.rotation, rotation, 0.10f);
        }
    }
}