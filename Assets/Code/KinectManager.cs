﻿using UnityEngine;
using System.Collections;
using Windows.Kinect;
using System.Collections.Generic;

public class KinectManager : MonoBehaviour 
{
    [System.Serializable]
    public class Configuration_Group
    {
        public GameObject   Avatar;                             // link to the avatar for model posing
        public GameObject   SpineBase;                          // link to important joint for copensatory movement tracking
        public GameObject   ActiveCamera;                       // link to perspective camera for projecting circle position onto screen space
        public bool         DisplayDebug;                       // determines if debug inforamtion is drawn to the screen
    }
    public Configuration_Group Configuration;

    [System.Serializable]
    public class HandTracking_Group
    {
        public bool         RightArmWeak;                       // specify which is the weak arm
        public float        MinimumHandSeperation   = 0.30f;    // required hand seperation for tracking
        public float        HandTrackingRadius      = 0.14f;    // radius in meters around the hand point to perform tracking
        public float        MiniumForarmAngle       = 45.0f;    // degrees that forarm must face the camera to enable tracking

        public float        HandClosedAvg           = 0.03f;    // value to register the hand as closed
        public float        HandOpenAvg             = 0.05f;    // value to register the hand as open

        public GameObject   HandMarker;                         // circle marker to display hand tracking state
        public GameObject   LeftHand;                           // gameobject to describe the position of the left hand
        public GameObject   RightHand;                          // gameobject to describe the position of the right hand
    }
    public HandTracking_Group HandTracking;

    [System.Serializable]
    public class ForwardsLean_Group
    {
        public float        Allowed                 = 10.0f;    // allowed angle before comensatory movement is detected
        public float        Max                     = 30.0f;    // maximum angle of comensatory limit
        public float        Offset                  = 15.0f;    // offset to comensate for sensor tilt

        public GameObject   WarningMarker;                      // marker used to display comensatory movement detection
        public GameObject   PositionObject;                     // object link to position display marker
    }
    public ForwardsLean_Group ForwardsLean;

    [System.Serializable]
    public class SidewaysLean_Group
    {
        public float        Allowed                 = 10.0f;    // allowed angle before comensatory movement is detected
        public float        Max                     = 30.0f;    // maximum angle of comensatory limit

        public GameObject   WarningMarker;                      // marker used to display comensatory movement detection
        public GameObject   PositionObject;                     // object link to position display marker
    }
    public SidewaysLean_Group SidewaysLean;

    [System.Serializable]
    public class ShoulderLean_Group
    {
        public float        Allowed                 = 10.0f;    // allowed angle before comensatory movement is detected
        public float        Max                     = 30.0f;    // maximum angle of comensatory limit

        public GameObject   WarningMarker;                      // marker used to display comensatory movement detection
        public GameObject   ShoulderLeft;                       // object link to position display marker
        public GameObject   ShoulderRight;                      // object link to position display marker
    }
    public ShoulderLean_Group ShoulderLean;


    private KinectSensor            _Sensor;                                // reference to the connected kinect v2 sensor
    private MultiSourceFrameReader  _Reader;                                // used to detect when new frame inforamton is ready
    private ushort[]                _DepthData;                             // array to store sensor depth inforamtion in mm
    private bool[]                  _VisitedData;                           // stores if a pixel has already been visited during controur searching
    private Body[]                  _BodyData;                              // store inforamtion about joint position and orientation for tracked bodies

    private int                     _FavouriteBodyIndex     = -1;           // body index of the player in the position we think is most likely to be the player
    private bool                    _IsHandTracked          = false;        // privatly indicates if the hand was sucesfully tracked during the last recived frame
    private float                   _HandOpenAmount         = 0.0f;         // privatly indicates the amount the hand is thought to be open
    private JointType               _WeakHand;                              // joint index indicating the weak hand
    private JointType               _StrongHand;                            // joint index indicating the strong hand
    private JointType               _WeakWrist;                             // joint index indicating the weak wrist
    private JointType               _WeakElbow;                             // joint index indicating the strong wrist


    private CameraSpacePoint[]      _CameraSpacePoints;                     // single allocation of cameraspacepoint required to overcome bug in single coordinate conversion routines
    private DepthSpacePoint[]       _DepthSpacePoints;                      // single allocation of depthspacepoint required to overcome bug in single coordinate conversion routines
    private ushort[]                _DepthMm;                               // single allocation of a depth value required to overcome bug in single coordinate conversion routines


    private DebugBufferRenderer     _DebugBuffer;                           // debug buffer to help rendering of hand tracking debug information
    private string                  _DebugString;                           // string to store any debug message to be printed to the screen

    public bool                     GetIsHandTracked { get { return _IsHandTracked; } }
    public float                    GetHandOpenAmount { get { return _HandOpenAmount; } }


    public Ship ship;


	void Start() 
    {
        _Sensor = KinectSensor.GetDefault();
        _Reader = _Sensor.OpenMultiSourceFrameReader(FrameSourceTypes.Depth | FrameSourceTypes.Body);

        FrameDescription depthFrameDesc = _Sensor.DepthFrameSource.FrameDescription;
        _DepthData = new ushort[depthFrameDesc.LengthInPixels];
        _VisitedData = new bool[depthFrameDesc.LengthInPixels];
        _DebugBuffer = new DebugBufferRenderer(depthFrameDesc.Width, depthFrameDesc.Height);

        if (!_Sensor.IsOpen)
        {
            _Sensor.Open();
        }

        for (int i = 0; i < _VisitedData.Length; i++)
        {
            _VisitedData[i] = false;
        }

        _CameraSpacePoints = new CameraSpacePoint[1];
        _DepthSpacePoints = new DepthSpacePoint[1];
        _DepthMm = new ushort[1];
	}

    void OnApplicationQuit()
    {
        if (_Reader != null)
        {
            _Reader.Dispose();
            _Reader = null;
        }

        if (_Sensor != null)
        {
            if (_Sensor.IsOpen)
            {
                _Sensor.Close();
            }

            _Sensor = null;
        }
    }
	
	void Update()
    {
        // pick important joint info based on the users weak arm
        if (HandTracking.RightArmWeak)
        {
            _WeakHand = JointType.HandRight;
            _StrongHand = JointType.HandLeft;
            _WeakWrist = JointType.WristRight;
            _WeakElbow = JointType.ElbowRight;
        }
        else
        {
            _WeakHand = JointType.HandLeft;
            _StrongHand = JointType.HandRight;
            _WeakWrist = JointType.WristLeft;
            _WeakElbow = JointType.ElbowLeft;
        }


        // check for any updates to kinect info
        bool frameUpdate = false;
        if (_Reader != null)
        {
            MultiSourceFrame frame = _Reader.AcquireLatestFrame();
            if (frame != null)
            {
                BodyFrame bodyFrame = frame.BodyFrameReference.AcquireFrame();
                if (bodyFrame != null)
                {
                    if (_BodyData == null)
                    {
                        _BodyData = new Body[_Sensor.BodyFrameSource.BodyCount];
                    }

                    bodyFrame.GetAndRefreshBodyData(_BodyData);

                    bodyFrame.Dispose();
                    bodyFrame = null;

                    frameUpdate = true;
                }


                DepthFrame depthFrame = frame.DepthFrameReference.AcquireFrame();
                if (depthFrame != null)
                {
                    depthFrame.CopyFrameDataToArray(_DepthData);

                    depthFrame.Dispose();
                    depthFrame = null;

                    frameUpdate = true;
                }

                frame.Dispose();
                frame = null;
            }
        }

        if (frameUpdate)
        {
            // find the body that is most likely to be the player
            float bestDistance = float.MaxValue;
            _FavouriteBodyIndex = -1;

            for (int i = 0; i < _Sensor.BodyFrameSource.BodyCount; i++)
            {
                if (_BodyData[i].IsTracked)
                {
                    float effectiveX = _BodyData[i].Joints[JointType.SpineBase].Position.X * 5f;
                    float effectiveZ = _BodyData[i].Joints[JointType.SpineBase].Position.Z;

                    if (new Vector2(effectiveX, effectiveZ).sqrMagnitude < bestDistance)
                    {
                        bestDistance = new Vector2(effectiveX, effectiveZ).sqrMagnitude;
                        _FavouriteBodyIndex = i;
                    }
                }
            }

            // draw the depth map to the debug buffer
            if (Configuration.DisplayDebug)
            {
                _DebugBuffer.ApplyDepthData(_DepthData);
            }

            // if we have no valid body, don't bother trying to do any tracking
            if (_FavouriteBodyIndex < 0)
            {
                return;
            }

            // now do body and hand tracking stats stuff, as well as recoring data etc etc...
            _IsHandTracked = TrackHand(ref _HandOpenAmount);	// should store some kind of status message return from this




            // *** control the ship here as a temp input ***
            float shipx = 0.0f;
            float shipy = 0.0f;

            if (HandTracking.RightArmWeak)
            {
                shipx = _BodyData[_FavouriteBodyIndex].Joints[JointType.HandRight].Position.X * 4f;
                shipy = _BodyData[_FavouriteBodyIndex].Joints[JointType.HandRight].Position.Y * 4f;
            }
            else
            {
                shipx = _BodyData[_FavouriteBodyIndex].Joints[JointType.HandLeft].Position.X * 4f;
                shipy = _BodyData[_FavouriteBodyIndex].Joints[JointType.HandLeft].Position.Y * 4f;
            }

            ship.ship_x = shipx;
            ship.ship_y = shipy;

            if (_HandOpenAmount < 0.2f)
            {
                ship.rotation = -90.0f;
            }
            else
            {
                ship.rotation = 0.0f;
            }




            // try and position the body model
            if (Configuration.Avatar != null)
            {
                BipedPoser biped = Configuration.Avatar.GetComponent<BipedPoser>();
                if (biped != null)
                {
                    foreach (PosableJoint joint in biped.joints)
                    {
                        joint.PoseJoint(_BodyData[_FavouriteBodyIndex]);
                    }
                }


                CircleMarkerManager handCircle = HandTracking.HandMarker.GetComponent<CircleMarkerManager>();
                if (_IsHandTracked)
                {
                    Vector3 handPosition = HandTracking.RightHand.transform.position;
                    if (HandTracking.RightArmWeak) { handPosition = HandTracking.LeftHand.transform.position; }

                    Vector3 handScreenPosition = WorldToOrtho(handPosition, Configuration.ActiveCamera.GetComponent<Camera>());
                    handScreenPosition.z = 15.0f;
                    HandTracking.HandMarker.transform.position = handScreenPosition;
                    HandTracking.HandMarker.transform.LookAt(handScreenPosition - new Vector3(0.0f, 0.0f, 1.0f));

                    handCircle.InnerRadius = 0.25f * _HandOpenAmount;
                    handCircle.OuterRadius = 0.25f * _HandOpenAmount + 0.1f;
                    handCircle.EdgeThickness = 0.15f;

                    handCircle.MainColour = new Color(1.0f, 1.0f, 1.0f, 0.25f);
                    handCircle.EdgeColour = new Color(1.0f, 1.0f, 1.0f, 0.5f);

                    handCircle.Enabled = true;
                }
                else
                {
                    handCircle.Enabled = false;
                }



                // *** COMPENSATORY ANGLE CALCUALTION AND VISUALIZATION *** //
                _DebugString = "";


                CameraSpacePoint ShoulderMidPoint = _BodyData[_FavouriteBodyIndex].Joints[JointType.SpineShoulder].Position;
                Vector3 ShoulderMidVector = new Vector3(ShoulderMidPoint.X, ShoulderMidPoint.Y, ShoulderMidPoint.Z);

                CameraSpacePoint HeadPoint = _BodyData[_FavouriteBodyIndex].Joints[JointType.Head].Position;
                Vector3 HeadVector = new Vector3(HeadPoint.X, HeadPoint.Y, HeadPoint.Z);

                CameraSpacePoint ShoulderPoint = HandTracking.RightArmWeak ? _BodyData[_FavouriteBodyIndex].Joints[JointType.ShoulderRight].Position : _BodyData[_FavouriteBodyIndex].Joints[JointType.ShoulderLeft].Position;
                Vector3 ShoulderVector = new Vector3(ShoulderPoint.X, ShoulderPoint.Y, ShoulderPoint.Z);

                float a = Vector3.Angle(HeadVector - ShoulderMidVector, ShoulderVector - ShoulderMidVector);
                _DebugString += a + "\n\n";




                float ForwardsLeanValue = AngleSigned(Vector3.forward, Quaternion.AngleAxis(-Configuration.SpineBase.transform.rotation.eulerAngles.y, Vector3.up) * Configuration.SpineBase.transform.forward, Vector3.right) + ForwardsLean.Offset;
                _DebugString += "FORWARDS LEAN:\n" + "Allowed: " + ForwardsLean.Allowed + "\nMax: " + ForwardsLean.Max + "\nCurrent: " + ForwardsLeanValue + "\n\n";

                float SidewaysLeanValue = AngleSigned(Configuration.SpineBase.transform.right, Quaternion.AngleAxis(Configuration.SpineBase.transform.rotation.eulerAngles.y, Vector3.up) * Vector3.right, Configuration.SpineBase.transform.forward);
                _DebugString += "SIDEWAYS LEAN:\n" + "Allowed: " + SidewaysLean.Allowed + "\nMax: " + SidewaysLean.Max + "\nCurrent: " + SidewaysLeanValue + "\n\n";

                float ShoulderLeanValue = AngleSigned(Vector3.forward, new Vector3(Configuration.SpineBase.transform.forward.x, 0.0f, Configuration.SpineBase.transform.forward.z), Vector3.up);
                _DebugString += "SHOULDER LEAN:\n" + "Allowed: " + ShoulderLean.Allowed + "\nMax: " + ShoulderLean.Max + "\nCurrent: " + ShoulderLeanValue + "\n\n";


                float ForwardsLeanAbs = Mathf.Clamp((Mathf.Abs(ForwardsLeanValue) - ForwardsLean.Allowed) / (ForwardsLean.Max - ForwardsLean.Allowed), 0.0f, 1.0f);
                float SidewaysLeanAbs = Mathf.Clamp((Mathf.Abs(SidewaysLeanValue) - SidewaysLean.Allowed) / (SidewaysLean.Max - SidewaysLean.Allowed), 0.0f, 1.0f);
                float ShoulderLeanAbs = Mathf.Clamp((Mathf.Abs(ShoulderLeanValue) - ShoulderLean.Allowed) / (ShoulderLean.Max - ShoulderLean.Allowed), 0.0f, 1.0f);


                CircleMarkerManager ForwardsWarningMarker = ForwardsLean.WarningMarker.GetComponent<CircleMarkerManager>();
                if (ForwardsLeanAbs > 0.0f)
                {
                    ForwardsWarningMarker.Enabled = true;

                    Vector3 ForwardsPosition = ForwardsLean.PositionObject.transform.position;
                    ForwardsPosition = WorldToOrtho(ForwardsPosition, Configuration.ActiveCamera.GetComponent<Camera>());
                    ForwardsPosition.z = 15.0f;
                    ForwardsWarningMarker.transform.position = ForwardsPosition;
                    ForwardsWarningMarker.transform.LookAt(ForwardsPosition - new Vector3(0.0f, 0.0f, 1.0f));

                    ForwardsWarningMarker.InnerRadius = 0.25f * ForwardsLeanAbs;
                    ForwardsWarningMarker.OuterRadius = 0.25f * ForwardsLeanAbs + 0.1f;
                    ForwardsWarningMarker.EdgeThickness = 0.15f;
                }
                else
                {
                    ForwardsWarningMarker.Enabled = false;
                }

                CircleMarkerManager SidewaysWarningMarker = SidewaysLean.WarningMarker.GetComponent<CircleMarkerManager>();
                if (SidewaysLeanAbs > 0.0f)
                {
                    SidewaysWarningMarker.Enabled = true;

                    Vector3 SidewaysPosition = SidewaysLean.PositionObject.transform.position;
                    SidewaysPosition = WorldToOrtho(SidewaysPosition, Configuration.ActiveCamera.GetComponent<Camera>());
                    SidewaysPosition.z = 15.0f;
                    SidewaysWarningMarker.transform.position = SidewaysPosition;
                    SidewaysWarningMarker.transform.LookAt(SidewaysPosition - new Vector3(0.0f, 0.0f, 1.0f));

                    SidewaysWarningMarker.InnerRadius = 0.25f * SidewaysLeanAbs;
                    SidewaysWarningMarker.OuterRadius = 0.25f * SidewaysLeanAbs + 0.1f;
                    SidewaysWarningMarker.EdgeThickness = 0.15f;
                }
                else
                {
                    SidewaysWarningMarker.Enabled = false;
                }

                CircleMarkerManager ShoulderWarningMarker = ShoulderLean.WarningMarker.GetComponent<CircleMarkerManager>();
                if (ShoulderLeanAbs > 0.0f)
                {
                    ShoulderWarningMarker.Enabled = true;

                    Vector3 ShoulderPosition = ShoulderLeanValue > 0.0f ? ShoulderLean.ShoulderLeft.transform.position : ShoulderLean.ShoulderRight.transform.position;
                    ShoulderPosition = WorldToOrtho(ShoulderPosition, Configuration.ActiveCamera.GetComponent<Camera>());
                    ShoulderPosition.z = 15.0f;
                    ShoulderWarningMarker.transform.position = ShoulderPosition;
                    ShoulderWarningMarker.transform.LookAt(ShoulderPosition - new Vector3(0.0f, 0.0f, 1.0f));

                    ShoulderWarningMarker.InnerRadius = 0.25f * ShoulderLeanAbs;
                    ShoulderWarningMarker.OuterRadius = 0.25f * ShoulderLeanAbs + 0.1f;
                    ShoulderWarningMarker.EdgeThickness = 0.15f;
                }
                else
                {
                    ShoulderWarningMarker.Enabled = false;
                }
            }
        }

	}

    Vector3 WorldToOrtho(Vector3 position, Camera camera)
    {
        Vector3 newpos = Configuration.ActiveCamera.GetComponent<Camera>().WorldToViewportPoint(position);
        newpos.x -= 0.5f;
        newpos.y -= 0.5f;
        newpos.x *= Camera.main.aspect * 2.0f;
        newpos.y *= 2.0f;

        return newpos;
    }

    public static float AngleSigned(Vector3 v1, Vector3 v2, Vector3 n)
    {
        return Mathf.Atan2(
            Vector3.Dot(n, Vector3.Cross(v1, v2)),
            Vector3.Dot(v1, v2)) * Mathf.Rad2Deg;
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawIcon(transform.position, "icon_manager.png", true);
    }

    void OnGUI()
    {
        if (Configuration.DisplayDebug)
        {
            /*
            _DebugBuffer.UpdateTexture();
            GUI.DrawTextureWithTexCoords(new Rect(0, 0,
                                                   _Sensor.DepthFrameSource.FrameDescription.Width,
                                                   _Sensor.DepthFrameSource.FrameDescription.Height),
                                         _DebugBuffer.GetTexture(),
                                         new Rect(0, 1, 1, -1),
                                         false);

            if (_FavouriteBodyIndex > -1)
            {
                CameraSpacePoint[] cameraSpacePoints = new CameraSpacePoint[2];
                DepthSpacePoint[] depthSpacePoints = new DepthSpacePoint[2];

                cameraSpacePoints[0] = _BodyData[_FavouriteBodyIndex].Joints[_WeakHand].Position;
                cameraSpacePoints[1] = _BodyData[_FavouriteBodyIndex].Joints[_StrongHand].Position;

                _Sensor.CoordinateMapper.MapCameraPointsToDepthSpace(cameraSpacePoints, depthSpacePoints);

                if (_BodyData[_FavouriteBodyIndex].Joints[_WeakHand].TrackingState == TrackingState.Tracked)
                {
                    GUI.Label(new Rect(depthSpacePoints[0].X + 20.0f, depthSpacePoints[0].Y + 20.0f, 300, 20), "Weak Hand");
                }

                if (_BodyData[_FavouriteBodyIndex].Joints[_StrongHand].TrackingState == TrackingState.Tracked)
                {
                    GUI.Label(new Rect(depthSpacePoints[1].X, depthSpacePoints[1].Y, 300, 20), "Strong Hand");
                }
            }
            */

            GUIStyle myStyle = new GUIStyle();
            myStyle.fontSize = 42;
            myStyle.normal.textColor = Color.white;
            GUI.Label(new Rect(10, 10, 1000, 1000), _DebugString, myStyle);
        }
    }

    bool TrackHand(ref float amount)
    {
        CameraSpacePoint weakHandPosition = _BodyData[_FavouriteBodyIndex].Joints[_WeakHand].Position;
        CameraSpacePoint strongHandPosition = _BodyData[_FavouriteBodyIndex].Joints[_StrongHand].Position;
        CameraSpacePoint weakWristPosition = _BodyData[_FavouriteBodyIndex].Joints[_WeakWrist].Position;
        CameraSpacePoint weakElbowPosition = _BodyData[_FavouriteBodyIndex].Joints[_WeakElbow].Position;

        Vector3 weakHandVector = new Vector3(weakHandPosition.X, weakHandPosition.Y, weakHandPosition.Z);
        Vector3 strongHandVector = new Vector3(strongHandPosition.X, strongHandPosition.Y, strongHandPosition.Z);
        Vector3 weakWristVector = new Vector3(weakWristPosition.X, weakWristPosition.Y, weakWristPosition.Z);
        Vector3 weakElbowVector = new Vector3(weakElbowPosition.X, weakElbowPosition.Y, weakElbowPosition.Z);

        DepthSpacePoint weakHandDepthPosition = MapCameraPointToDepthSpace(weakHandPosition);
        DepthSpacePoint strongHandDepthPosition = MapCameraPointToDepthSpace(strongHandPosition);
        DepthSpacePoint weakWristDepthPosition = MapCameraPointToDepthSpace(weakWristPosition);
        DepthSpacePoint weakElbowDepthPosition = MapCameraPointToDepthSpace(weakElbowPosition);


        // check that we have a good tracking position on the hand
        if (_BodyData[_FavouriteBodyIndex].Joints[_WeakHand].TrackingState == TrackingState.NotTracked)
        {
            return false;
        }

        // check if we have returned a valid coordinate
        if (weakHandDepthPosition.X == float.NaN || weakHandDepthPosition.Y == float.NaN)
        {
            return false;
        }

        // check for valid wrist
        if (_BodyData[_FavouriteBodyIndex].Joints[_WeakWrist].TrackingState == TrackingState.NotTracked)
        {
            return false;
        }

        // check for valid elbow
        if (_BodyData[_FavouriteBodyIndex].Joints[_WeakElbow].TrackingState == TrackingState.NotTracked)
        {
            return false;
        }

        // check if the hands are a good distance apart
        if (_BodyData[_FavouriteBodyIndex].Joints[_StrongHand].TrackingState != TrackingState.NotTracked)
        {
            if ((weakHandVector - strongHandVector).magnitude < HandTracking.MinimumHandSeperation)
            {
                return false;
            }
        }


        // check that the forearm sufficently faces the camera
        if (Vector3.Angle((weakHandVector - weakElbowVector).normalized, new Vector3(0.0f, 0.0f, -1.0f)) > HandTracking.MiniumForarmAngle)
        {
            return false;
        }


        // create a box around the hand area
        FrameDescription depthFrameDesc = _Sensor.DepthFrameSource.FrameDescription;
        float metersX = Mathf.Tan(Mathf.Deg2Rad * (depthFrameDesc.HorizontalFieldOfView / 2.0f)) * 2.0f;
        float pixlesPerMeter = (float)depthFrameDesc.Width / metersX;
        int boxHalfEdge = (int)((pixlesPerMeter * HandTracking.HandTrackingRadius) / weakHandPosition.Z);

        int left = (int)weakHandDepthPosition.X - boxHalfEdge;
        int right = left + boxHalfEdge + boxHalfEdge;
        int top = (int)weakHandDepthPosition.Y - boxHalfEdge;
        int bottom = top + boxHalfEdge + boxHalfEdge;

        // check that the box is inside the FOV
        if (left < 0 || top < 0 || right >= depthFrameDesc.Width || bottom >= depthFrameDesc.Height)
        {
            if (Configuration.DisplayDebug)
            {
                _DebugBuffer.DrawBox(left, top, right, bottom, Color.yellow);
            }
            return false;
        }
        else
        {
            if (Configuration.DisplayDebug)
            {
                _DebugBuffer.DrawBox(left, top, right, bottom, Color.red);
            }
        }

        // create boundary around hand region to prevent contrours leaking
        #region
        int index = left + top * depthFrameDesc.Width;
        for (int i = left; i < right; i++)
        {
            _DepthData[index++] = 0;
        }

        index = left + bottom * depthFrameDesc.Width;
        for (int i = left; i < right; i++)
        {
            _DepthData[index++] = 0;
        }

        index = top * depthFrameDesc.Width + left;
        for (int i = top; i < bottom; i++)
        {
            _DepthData[index] = 0;
            index = index + depthFrameDesc.Width;
        }

        index = top * depthFrameDesc.Width + right;
        for (int i = top; i < bottom; i++)
        {
            _DepthData[index] = 0;
            index = index + depthFrameDesc.Width;
        }
        #endregion


        // scan though the active area looking for places to start scanning
        List<List<ContourPoint>> contours = new List<List<ContourPoint>>();

        float HandTrackingRadiusSqrd = HandTracking.HandTrackingRadius * HandTracking.HandTrackingRadius;
        bool onVisitedPixel = false; // begin in mode searching for new pixel

        for (int y = top + 1; y < bottom; y++)
        {
            // at the begining of each line we are not on a shape
            onVisitedPixel = false;

            int offset_index = y * depthFrameDesc.Width;
            for (int i = left + 1 + offset_index; i < right + offset_index; i++)
            {
                DepthSpacePoint depthSpacePoint;
                depthSpacePoint.X = (float)(i - offset_index);
                depthSpacePoint.Y = (float)(y);

                CameraSpacePoint depthPixelPointInCameraSpace = MapDepthPointToCameraSpace(depthSpacePoint, _DepthData[i]);
                Vector3 pixel = new Vector3(depthPixelPointInCameraSpace.X, depthPixelPointInCameraSpace.Y, depthPixelPointInCameraSpace.Z);

                if (onVisitedPixel)
                {
                    // check if we are leaving the current shape
                    if ((weakHandVector - pixel).sqrMagnitude >= HandTrackingRadiusSqrd)
                    {
                        // go back into search mode
                        onVisitedPixel = false;
                    }
                }
                else
                {
                    // cehck if the pixel is on a shape
                    if ((weakHandVector - pixel).sqrMagnitude < HandTrackingRadiusSqrd)
                    {
                        // check if we have already mapped this shape
                        if (!_VisitedData[i])
                        {
                            // found a new shape, trace the contour
                            List<ContourPoint> contour = TraceContour(i, weakHandVector, HandTrackingRadiusSqrd, _DepthData,
                                         _VisitedData, depthFrameDesc.Width, depthFrameDesc.Height);

                            if (contour != null)
                            {
                                contours.Add(contour);
                            }
                        }

                        onVisitedPixel = true;
                    }
                }
            }
        }


        // check we found any contours
        if (contours.Count < 1)
        {
            return false;
        }

        // find the largest contour
        List<ContourPoint> longestContour = contours[0];
        foreach (List<ContourPoint> contour in contours)
        {
            if (longestContour.Count < contour.Count)
            {
                longestContour = contour;
            }

            if (Configuration.DisplayDebug)
            {
                _DebugBuffer.DrawContour(contour, Color.gray);

            }
        }

        if (Configuration.DisplayDebug)
        {
            //_DebugString = longestContour.Count.ToString();

            float[] countourDistance = new float[longestContour.Count];
            float totalDistance = 0.0f;
            int countValidPoints = 0;
            index = 0;

            foreach (ContourPoint point in longestContour)
            {
                DepthSpacePoint depthPoint = new DepthSpacePoint();
                depthPoint.X = point.position.x;
                depthPoint.Y = point.position.y;

                CameraSpacePoint cameraPoint = MapDepthPointToCameraSpace(depthPoint, _DepthData[point.index]);
                Vector3 pointVector = new Vector3(cameraPoint.X, cameraPoint.Y, cameraPoint.Z);

                _DebugBuffer.PlotPixel(point.index, Color.yellow);

                float distance = (pointVector - weakHandVector).magnitude;
                countourDistance[index++] = distance;

                totalDistance += distance;
                countValidPoints++;
            }


            float average = totalDistance / (float)countValidPoints;
            float totalDisturbance = 0.0f;
            index = 0;

            foreach (float distance in countourDistance)
            {
                int intDist = (int)(distance * 250);
                int intAvg = (int)(average * 250);

                if (distance > 0.0f)
                {
                    int min = intDist < intAvg ? intDist : intAvg;
                    int max = intDist > intAvg ? intDist : intAvg;

                    for (int j = min; j < max; j++)
                    {
                        _DebugBuffer.PlotPixel(index + j * depthFrameDesc.Width, Color.green);
                    }

                    _DebugBuffer.PlotPixel(index + intAvg * depthFrameDesc.Width, Color.red);
                    _DebugBuffer.PlotPixel(index++ + intDist * depthFrameDesc.Width, Color.yellow);

                    totalDisturbance += Mathf.Abs(distance - average);
                }
                else
                {
                    _DebugBuffer.PlotPixel(index + intAvg * depthFrameDesc.Width, Color.gray);
                    _DebugBuffer.PlotPixel(index++ * depthFrameDesc.Width, Color.gray);
                }
            }
        }


        // work out if the hand is open
        float maxOpen = 300.0f / weakHandVector.z;
        float minClose = maxOpen / 2.0f;
        float currentHand = ((float)longestContour.Count - minClose) / (maxOpen - minClose);

        //_DebugString = currentHand.ToString("000.000");

        if (currentHand < 0.0f) { currentHand = 0.0f; }
        if (currentHand > 1.0f) { currentHand = 1.0f; }

        amount = Mathf.Lerp(amount, currentHand, 0.25f);   // pass back how open the hand is

        // display important joint positions for debug
        if (Configuration.DisplayDebug)
        {
            _DebugBuffer.DrawBox((int)weakElbowDepthPosition.X - 5, (int)weakElbowDepthPosition.Y - 5,
                                  (int)weakElbowDepthPosition.X + 5, (int)weakElbowDepthPosition.Y + 5,
                                  Color.gray);

            _DebugBuffer.DrawBox((int)weakHandDepthPosition.X - 5, (int)weakHandDepthPosition.Y - 5,
                                  (int)weakHandDepthPosition.X + 5, (int)weakHandDepthPosition.Y + 5,
                                  Color.gray);

            _DebugBuffer.DrawBox((int)weakWristDepthPosition.X - 5, (int)weakWristDepthPosition.Y - 5,
                                  (int)weakWristDepthPosition.X + 5, (int)weakWristDepthPosition.Y + 5,
                                  Color.gray);
        }


        // look though all contours and set their visited data back to false
        foreach (List<ContourPoint> contour in contours)
        {
            foreach (ContourPoint point in contour)
            {
                _VisitedData[point.index] = false;
            }
        }

        // report sucessfull hand tracking
        return true;
    }

    private DepthSpacePoint MapCameraPointToDepthSpace(CameraSpacePoint cameraSpacePoint)
    {
        _CameraSpacePoints[0] = cameraSpacePoint;
        _Sensor.CoordinateMapper.MapCameraPointsToDepthSpace(_CameraSpacePoints, _DepthSpacePoints);
        return _DepthSpacePoints[0];
    }

    private CameraSpacePoint MapDepthPointToCameraSpace(DepthSpacePoint depthSpacePoint, ushort depthMm)
    {
        _DepthSpacePoints[0] = depthSpacePoint;
        _DepthMm[0] = depthMm;
        _Sensor.CoordinateMapper.MapDepthPointsToCameraSpace(_DepthSpacePoints, _DepthMm, _CameraSpacePoints);
        return _CameraSpacePoints[0];
    }

    private List<ContourPoint> TraceContour(int startingIndex, Vector3 handPosition, float HandTrackingRadiusSqrd, ushort[] depthData, bool[] visitedPixelBuffer, int width, int height)
    {
        // order of chose direction of next movement in a clockwise circle
        int[] direction = new int[16];
        direction[0] = -1 - width;
        direction[1] = -width;
        direction[2] = -width + 1;
        direction[3] = 1;
        direction[4] = 1 + width;
        direction[5] = width;
        direction[6] = width - 1;
        direction[7] = -1;

        direction[8] = direction[0];
        direction[9] = direction[1];
        direction[10] = direction[2];
        direction[11] = direction[3];
        direction[12] = direction[4];
        direction[13] = direction[5];
        direction[14] = direction[6];
        direction[15] = direction[7];


        List<ContourPoint> points = new List<ContourPoint>();
        int currentIndex = startingIndex;
        int currentDirection = 0;

        DepthSpacePoint depthSpacePoint;
        CameraSpacePoint depthPixelPointInCameraSpace;
        ContourPoint contourPoint;

        do
        {
            visitedPixelBuffer[currentIndex] = true;

            while (true)
            {
                int nextIndex = currentIndex + direction[currentDirection];

                depthSpacePoint.X = (float)(nextIndex & 0x01FF);
                depthSpacePoint.Y = (float)(nextIndex >> 9);

                depthPixelPointInCameraSpace = MapDepthPointToCameraSpace(depthSpacePoint, _DepthData[nextIndex]);
                Vector3 pixel = new Vector3(depthPixelPointInCameraSpace.X, depthPixelPointInCameraSpace.Y, depthPixelPointInCameraSpace.Z);

                // see if we can find another pixel on the shape
                if ((handPosition - pixel).sqrMagnitude < HandTrackingRadiusSqrd)
                {
                    // we found something add it to the contour
                    currentIndex = nextIndex;
                    currentDirection = (currentDirection + 6) % 8;

                    contourPoint.x = currentIndex & 0x01FF;
                    contourPoint.y = currentIndex >> 9;
                    contourPoint.index = currentIndex;
                    contourPoint.position = new Vector2((float)contourPoint.x, (float)contourPoint.y);

                    points.Add(contourPoint);
                    visitedPixelBuffer[currentIndex] = true;

                    break;
                }

                // found nothing change direction and look again
                currentDirection++;

                // if we have found a single point, ignore and move on
                if (currentDirection > 15)
                {
                    return null;
                }
            }
        }
        while (currentIndex != startingIndex);

        return points;
    }
}
