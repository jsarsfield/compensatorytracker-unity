﻿using UnityEngine;
using System.Collections;
using UnityEditor;


[CustomEditor(typeof(BipedPoser))]
public class BipedPoserEditor : Editor
{
    public BipedPoserEditor()
    {
        
    }

    public override void OnInspectorGUI()
    {
        Vector2 inspector_width = GUI.skin.GetStyle("Toggle").CalcSize(new GUIContent());
        PosableJoint[] joints = ((BipedPoser)target).joints;
        EditorGUILayout.Separator();

        foreach (PosableJoint joint in joints)
        {
            EditorGUILayout.BeginHorizontal();
            joint.editor_tab_open = EditorGUILayout.Foldout(joint.editor_tab_open, joint.bone_name);
            joint.editor_display_gizmo = EditorGUILayout.Toggle("", joint.editor_display_gizmo, GUILayout.Width(inspector_width.x));
            EditorGUILayout.EndHorizontal();

            if (joint.editor_tab_open)
            {
                EditorGUI.indentLevel++;

                joint.posing_enabled = EditorGUILayout.Toggle("Enabled", joint.posing_enabled);
                joint.bone = (GameObject)EditorGUILayout.ObjectField("Bone", joint.bone, typeof(GameObject));


                GUI.enabled = false;
                if (joint.bone != null)
                {
                    GUI.enabled = true;
                }
                joint.rotation_offset = Quaternion.Euler(EditorGUILayout.Vector3Field("Rotation", joint.rotation_offset.eulerAngles));


                EditorGUI.indentLevel++;
                joint.target = (GameObject)EditorGUILayout.ObjectField("Target", joint.target, typeof(GameObject));
                

                GUI.enabled = false;
                if (joint.has_bone_and_target)
                {
                    if (joint.has_valid_direction)
                    {
                        GUI.enabled = true;
                    }
                    else
                    {
                        EditorGUILayout.HelpBox("Bone and Target cannot share same position.", MessageType.None, false);
                    }
                }

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.PrefixLabel("Auto");
                if (GUILayout.Button("Recalculate", GUILayout.Width(100)))
                {
                    //Vector3 target_up = ((PoseCreator)target).transform.rotation * joint.look_up;
                    //joint.rotation_offset = Quaternion.LookRotation(joint.target.transform.position - joint.bone.transform.position, target_up);
                    joint.AutoComputeRotation();
                }
                EditorGUILayout.EndHorizontal();

                GUI.enabled = true;


                EditorGUI.indentLevel--;
                EditorGUI.indentLevel--;
            }
        }


        EditorGUILayout.Separator();
        EditorGUILayout.HelpBox("Warning: 'Auto Pose' will destroy any pre exisiting limb orientation data.", MessageType.Info, true);
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.PrefixLabel("Auto");
        if (GUILayout.Button("Auto Pose", GUILayout.Width(100)))
        {
            foreach (PosableJoint joint in joints)
            {
                joint.bone = null;
                joint.target = null;
                joint.rotation_offset = Quaternion.identity;
                joint.posing_enabled = false;
                joint.editor_display_gizmo = false;

                GameObject bone = FindBone(((BipedPoser)target).transform, joint.bone_name);
                if (bone != null)
                {
                    joint.bone = bone;

                    GameObject target_bone = FindBone(((BipedPoser)target).transform, joint.target_name);
                    if (target_bone != null)
                    {
                        joint.target = target_bone;

                        if (joint.has_valid_direction)
                        {
                            Vector3 target_up = ((BipedPoser)target).transform.rotation * joint.look_up;
                            joint.rotation_offset = Quaternion.LookRotation(joint.target.transform.position - joint.bone.transform.position, target_up) * 
                                Quaternion.AngleAxis(90, new Vector3(1, 0, 0));

                            joint.posing_enabled = true;
                            joint.editor_display_gizmo = true;
                        }
                    }
                }

                joint.editor_tab_open = true;
            }
        }
        EditorGUILayout.EndHorizontal();


        EditorUtility.SetDirty(target);
    }

    public void OnSceneGUI()
    {
        PosableJoint[] joints = ((BipedPoser)target).joints;
        foreach (PosableJoint joint in joints)
        {
            if (joint.editor_display_gizmo && joint.bone != null)
            {
                Handles.PositionHandle(joint.bone.transform.position, joint.rotation_offset);
                Handles.Label(joint.bone.transform.position, joint.bone_name);
            }
        }
    }

    private GameObject FindBone(Transform transform, string name)
    {
        foreach (Transform trans in transform)
        {
            if (trans.name == name)
            {
                return trans.gameObject;
            }
            else
            {
                GameObject down = FindBone(trans, name);
                if (down != null)
                {
                    return down;
                }
            }
        }

        return null;
    }
}