﻿using UnityEngine;
using System.Collections;
using Windows.Kinect;

public class BodySourceManager : MonoBehaviour 
{
    private KinectSensor _Sensor;
    private BodyFrameReader _Reader;
	private Body[] _Data = null;								// store inforamtion about joint position and orientation for tracked bodies

	[System.Serializable]
	public class Configuration_Group
	{
		public GameObject   Avatar;                             // link to the avatar for model posing
	}

	public Configuration_Group Configuration;
    
    public Body[] GetData()
    {
        return _Data;
    }
    

    void Start () 
    {
        _Sensor = KinectSensor.GetDefault();

        if (_Sensor != null)
        {
            _Reader = _Sensor.BodyFrameSource.OpenReader();
            
            if (!_Sensor.IsOpen)
            {
                _Sensor.Open();
            }
        }   
    }
    
    void Update () 
    {
        if (_Reader != null)
        {
            var frame = _Reader.AcquireLatestFrame();
            if (frame != null)
            {
                if (_Data == null)
                {
                    _Data = new Body[_Sensor.BodyFrameSource.BodyCount];
                }
                
                frame.GetAndRefreshBodyData(_Data);

				// try and position the body model
				if (Configuration.Avatar != null)
				{
					BipedPoser biped = Configuration.Avatar.GetComponent<BipedPoser>();
					if (biped != null)
					{
						foreach (PosableJoint joint in biped.joints)
						{
							joint.PoseJoint(_Data[0]);
						}
					}
				}
                
                frame.Dispose();
                frame = null;
            }
        }    
    }
    
    void OnApplicationQuit()
    {
        if (_Reader != null)
        {
            _Reader.Dispose();
            _Reader = null;
        }
        
        if (_Sensor != null)
        {
            if (_Sensor.IsOpen)
            {
                _Sensor.Close();
            }
            
            _Sensor = null;
        }
    }
}
