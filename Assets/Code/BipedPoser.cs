﻿using UnityEngine;
using System.Collections;
using Windows.Kinect;


public class BipedPoser : MonoBehaviour 
{
    public PosableJoint[] joints = new PosableJoint[] {
        new PosableJoint((int)JointType.SpineBase,      "SPINE_BASE",       "SPINE_MID",        new Vector3( 0, 0, 1)),
        new PosableJoint((int)JointType.SpineMid,       "SPINE_MID",        "SPINE_SHOULDER",   new Vector3( 0, 0, 1)),
        new PosableJoint((int)JointType.KneeRight,      "HIP_LEFT",         "KNEE_LEFT",        new Vector3(-1, 0, 0)),
        new PosableJoint((int)JointType.AnkleRight,     "KNEE_LEFT",        "ANKLE_LEFT",       new Vector3(-1, 0, 0)),
        new PosableJoint((int)JointType.KneeLeft,       "HIP_RIGHT",        "KNEE_RIGHT",       new Vector3( 1, 0, 0)),
        new PosableJoint((int)JointType.AnkleLeft,      "KNEE_RIGHT",       "ANKLE_RIGHT",      new Vector3( 1, 0, 0)),
        new PosableJoint((int)JointType.ShoulderRight,  "COLAR_LEFT",       "SHOULDER_LEFT",    new Vector3( 0, 0, 1)),
        new PosableJoint((int)JointType.ElbowRight,     "SHOULDER_LEFT",    "ELBOW_LEFT",       new Vector3( 0, 1, 0)),
        new PosableJoint((int)JointType.WristRight,     "ELBOW_LEFT",       "WRIST_LEFT",       new Vector3( 0, 0, 1)),
        new PosableJoint((int)JointType.HandRight,      "WRIST_LEFT",       "HAND_LEFT",        new Vector3( 0, 0, 1)),
        new PosableJoint((int)JointType.ShoulderLeft,   "COLAR_RIGHT",      "SHOULDER_RIGHT",   new Vector3( 0, 0, 1)),
        new PosableJoint((int)JointType.ElbowLeft,      "SHOULDER_RIGHT",   "ELBOW_RIGHT",      new Vector3( 0, 1, 0)),
        new PosableJoint((int)JointType.WristLeft,      "ELBOW_RIGHT",      "WRIST_RIGHT",      new Vector3( 0, 0, 1)),
        new PosableJoint((int)JointType.HandLeft,       "WRIST_RIGHT",      "HAND_RIGHT",       new Vector3( 0, 0, 1))
    };

    void Awake()
    {
        foreach (PosableJoint joint in joints)
        {
            joint.CreateHelperBone();
        }
    }

 	void Start () 
    {
        
	}

    void Update() 
    {

	}
}
