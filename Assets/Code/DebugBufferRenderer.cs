﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public struct ContourPoint
{
	public int x;
	public int y;
	public int index;
	public Vector2 position;
};

public class DebugBufferRenderer
{
	private Color32[] _Buffer;
	private Texture2D _Texture;
	private int _Width;
	private int _Height;

	public DebugBufferRenderer(int width, int height)
	{
		_Buffer = new Color32[width * height];
		_Texture = new Texture2D (width, height, TextureFormat.RGBA32, false);
		_Width = width;
		_Height = height;
	}

	public void UpdateTexture()
	{
		_Texture.SetPixels32 (_Buffer);
		_Texture.Apply ();
	}

	public Texture2D GetTexture()
	{
		return _Texture;
	}

	public void ApplyDepthData(ushort[] depthData)
	{
		for (int i = 0; i < depthData.Length; i++)
		{
			byte value = (byte)(depthData[i] >> 6);
			_Buffer[i] = new Color32(value, value, value, 0xFF);
		}
	}

	public void DrawBox(int left, int top, int right, int bottom, Color colour)
	{
		Color32 fast_colour = colour;

		if (top >= 0 && top < _Height) 
		{
			int local_left = left < 0 ? 0 : left;
			int local_right = right > _Width ? _Width : right;
			int index = local_left + top * _Width;

			for (int i = local_left; i < local_right; i++)
			{
				_Buffer[index++] = fast_colour;
			}
		}

		if (bottom >= 0 && bottom < _Height) 
		{
			int local_left = left < 0 ? 0 : left;
			int local_right = right > _Width ? _Width : right;
			int index = local_left + bottom * _Width;
			
			for (int i = local_left; i < local_right; i++)
			{
				_Buffer[index++] = colour;
			}
		}

		if (left >= 0 && left < _Width) 
		{
			int local_top = top < 0 ? 0 : top;
			int local_bottom = bottom > _Height ? _Height : bottom;
			int index = local_top * _Width + left;
			
			for (int i = local_top; i < local_bottom; i++)
			{
				_Buffer[index] = fast_colour;
				index = index + _Width;
			}
		}

		if (right >= 0 && right < _Width) 
		{
			int local_top = top < 0 ? 0 : top;
			int local_bottom = bottom > _Height ? _Height : bottom;
			int index = local_top * _Width + right;
			
			for (int i = local_top; i < local_bottom; i++)
			{
				_Buffer[index] = fast_colour;
				index = index + _Width;
			}
		}
	}

	public void PlotPixel(int index, Color32 colour)
	{
		_Buffer [index] = colour;
	}

	public void DrawContour(List<ContourPoint> contour, Color32 colour)
	{
		foreach (ContourPoint point in contour)
		{
			_Buffer[point.index] = colour;
		}
	}
}
