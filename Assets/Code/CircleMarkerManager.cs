﻿using UnityEngine;
using System.Collections;

public class CircleMarkerManager : MonoBehaviour 
{
    private CircleGenerator MainCircle;
    private CircleGenerator OuterEdge;
    private CircleGenerator InnerEdge;

    public Color MainColour;
    public Color EdgeColour;
    public float InnerRadius;
    public float OuterRadius;
    public float EdgeThickness;

    public bool Enabled = false;

	void Start () 
    {
        CircleGenerator[] circleGenerators = gameObject.GetComponentsInChildren<CircleGenerator>();
        MainCircle = circleGenerators[0];
        OuterEdge = circleGenerators[1];
        InnerEdge = circleGenerators[2];
	}
	
	void Update () 
    {
        if (Enabled)
        {
            MainCircle.inner_radius = InnerRadius;
            MainCircle.outer_radius = OuterRadius;

            OuterEdge.inner_radius = OuterRadius - EdgeThickness / 2;
            OuterEdge.outer_radius = OuterRadius + EdgeThickness / 2;

            InnerEdge.inner_radius = InnerRadius - EdgeThickness / 2;
            InnerEdge.outer_radius = InnerRadius + EdgeThickness / 2;


            MainCircle.renderer.material.color = MainColour;
            OuterEdge.renderer.material.color = EdgeColour;
            InnerEdge.renderer.material.color = EdgeColour;

            MainCircle.renderer.enabled = true;
            OuterEdge.renderer.enabled = true;
            InnerEdge.renderer.enabled = true;
        }
        else
        {
            MainCircle.renderer.enabled = false;
            OuterEdge.renderer.enabled = false;
            InnerEdge.renderer.enabled = false;
        }
	}
}
