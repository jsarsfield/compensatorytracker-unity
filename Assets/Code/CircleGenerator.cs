﻿using UnityEngine;
using System.Collections;

public class CircleGenerator : MonoBehaviour 
{
    public int segments = 32;
    public float inner_radius = 1.0f;
    public float outer_radius = 1.2f;

    private MeshFilter mesh_filter;


	void Start() 
    {
        mesh_filter = gameObject.GetComponent<MeshFilter>();
	}
	
	void Update()
    {
        /*
        float time = Time.realtimeSinceStartup / 4.0f;
        time = time - (float)(int)time;
        time = time * 2.0f;
        if (time > 1.0f) { time = 2.0f - time; }

        float value = Mathf.Lerp(1.0f, 0.2f, time * time * (3.0f - 2.0f * time));
        inner_radius = value;
        outer_radius = value + 0.2f;
        */
        UpdateMesh();

        //renderer.material.color = new Color(1.0f, 0.0f, 0.0f, time);    // change colour for transparency
	}

    public void UpdateMesh()
    {
        Mesh mesh = mesh_filter.sharedMesh;
        if (mesh == null)
        {
            mesh_filter.mesh = new Mesh();
            mesh = mesh_filter.sharedMesh;
        }

        Vector3[] vertices = new Vector3[segments * 2];
        int[] indices = new int[segments * 2 * 3];
        float step = 360.0f / (float)segments;

        for (int i = 0; i < segments; i++)
        {
            Vector3 unit_direction = Quaternion.AngleAxis(step * (float)i, Vector3.forward) * Vector3.up;
            vertices[i] = unit_direction * inner_radius;
            vertices[i + segments] = unit_direction * outer_radius;

            int next = (i + 1) % segments;

            indices[i * 6 + 0] = i + segments;
            indices[i * 6 + 1] = next;
            indices[i * 6 + 2] = i;

            indices[i * 6 + 3] = i + segments;
            indices[i * 6 + 4] = next + segments;
            indices[i * 6 + 5] = next;
        }

        Vector2[] texcoords = new Vector2[segments * 2];
        Vector4[] tangents = new Vector4[segments * 2];
        for (int i = 0; i < segments * 2; i++)
        {
            texcoords[i] = Vector2.zero;
            tangents[i] = Vector4.zero;
        }

        mesh.Clear();
        mesh.vertices = vertices;
        mesh.uv = texcoords;
        mesh.tangents = tangents;
        mesh.triangles = indices;

        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
    }
}