﻿using UnityEngine;
using System.Collections;


public struct TrackVert
{
    public Vector2 position;
    public float uvx;

    public TrackVert(Vector2 position, float uvx)
    {
        this.position = position;
        this.uvx = uvx;
    }
}

[RequireComponent(typeof(MeshFilter))]
public class TrackPiece : MonoBehaviour
{
    private static int[] _indices;
    private static Vector2[] _texcoords;
    private static Vector3[] vertices;


    private const int _segments = 100;
    private const int _splits = _segments + 1;

    private static TrackVert[] trackcrosssection = {
            new TrackVert( new Vector2( 6.0f,-1.0f), 0.0f ), new TrackVert( new Vector2( 7.0f, 0.0f), 1.0f ),
            new TrackVert( new Vector2( 7.0f, 0.0f), 0.0f ), new TrackVert( new Vector2( 7.0f, 3.0f), 3.0f ),
            new TrackVert( new Vector2( 7.0f, 3.0f), 0.0f ), new TrackVert( new Vector2( 5.0f, 3.0f), 2.0f ),
            new TrackVert( new Vector2( 5.0f, 3.0f), 0.0f ), new TrackVert( new Vector2( 5.0f, 1.0f), 2.0f ),
            new TrackVert( new Vector2( 5.0f, 1.0f), 0.0f ), new TrackVert( new Vector2( 4.0f, 0.0f), 1.0f ),
            new TrackVert( new Vector2( 4.0f, 0.0f), 0.0f ), new TrackVert( new Vector2(-4.0f, 0.0f), 8.0f ),
            new TrackVert( new Vector2(-4.0f, 0.0f), 0.0f ), new TrackVert( new Vector2(-5.0f, 1.0f), 1.0f ),
            new TrackVert( new Vector2(-5.0f, 1.0f), 0.0f ), new TrackVert( new Vector2(-5.0f, 3.0f), 2.0f ),
            new TrackVert( new Vector2(-5.0f, 3.0f), 0.0f ), new TrackVert( new Vector2(-7.0f, 3.0f), 2.0f ),
            new TrackVert( new Vector2(-7.0f, 3.0f), 0.0f ), new TrackVert( new Vector2(-7.0f, 0.0f), 3.0f ),
            new TrackVert( new Vector2(-7.0f, 0.0f), 0.0f ), new TrackVert( new Vector2(-6.0f,-1.0f), 1.0f )
        };

    private static int[] trackindices = {
            0,  1,
            2,  3,
            4,  5,
            6,  7,
            8,  9,
            10, 11,
            12, 13,
            14, 15,
            16, 17,
            18, 19
        };


    static TrackPiece()
    {
        vertices = new Vector3[trackcrosssection.Length * _splits];
        _texcoords = new Vector2[trackcrosssection.Length * _splits];
        _indices = new int[trackindices.Length * 3 * _segments];


        for (int i = 0; i < _splits; i++)
        {
            int offset = i * trackcrosssection.Length;
            for (int j = 0; j < trackcrosssection.Length; j++)
            {
                _texcoords[offset + j] = new Vector2(trackcrosssection[j].uvx, (float)i);
            }
        }


        for (int i = 0; i < _segments; i++)
        {
            int offset = i * trackindices.Length * 3;
            int vertexoffset = trackcrosssection.Length * i;
            int vertnextrow = vertexoffset + trackcrosssection.Length;

            for (int j = 0; j < trackindices.Length; j += 2)
            {
                int joffset = offset + j * 3;

                _indices[joffset + 0] = vertexoffset + trackindices[j + 1];
                _indices[joffset + 1] = vertexoffset + trackindices[j];
                _indices[joffset + 2] = vertnextrow + trackindices[j];

                _indices[joffset + 3] = vertexoffset + trackindices[j + 1];
                _indices[joffset + 4] = vertnextrow + trackindices[j];
                _indices[joffset + 5] = vertnextrow + trackindices[j + 1];
            }
        }
    }


    private Vector3 _p0;
    private Vector3 _p1;
    private Vector3 _p2;

    private Mesh _mesh;

    private float _length;
    public float length { get { return _length; } }


    TrackPiece() { }


    void Awake()
    {
        _mesh = GetComponent<MeshFilter>().mesh;
    }

    void Update()
    {

    }

    public void GenerateTrack(Vector3 p0, Vector3 p1, Vector3 p2)
    {
        _p0 = p0;
        _p1 = p1;
        _p2 = p2;


        for (int i = 0; i < _splits; i++)
        {
            Vector3 point = GetPoint((float)(i) / (float)(_splits - 1));
            Vector3 forward;

            if (i == 0)
            {
                forward = p1 - p0;
            }
            else if (i == _splits - 1)
            {
                forward = p2 - p1;
            }
            else
            {
                Vector3 prevpoint = GetPoint((float)(i - 1) / (float)_splits);
                Vector3 nextpoint = GetPoint((float)(i + 1) / (float)_splits);
                forward = nextpoint - prevpoint;

            }

            Vector3 sideways = new Vector3(-forward.z, 0f, forward.x);
            sideways.Normalize();


            int offset = i * trackcrosssection.Length;
            for (int j = 0; j < trackcrosssection.Length; j++)
            {
                Vector3 vert = point + trackcrosssection[j].position.x * sideways;
                vert.y += trackcrosssection[j].position.y;
                vertices[offset + j] = vert;
            }
        }

        _mesh.Clear();
        _mesh.vertices = vertices;
        _mesh.uv = _texcoords;
        _mesh.triangles = _indices;
        _mesh.RecalculateNormals();
        _mesh.RecalculateBounds();


        Vector3 prev_point = _p0;
        _length = 0.0f;
        for (int i = 1; i < 30; i++)
        {
            Vector3 point = GetPoint((float)(i) / (float)(30 - 1));
            _length += (point - prev_point).magnitude;
            prev_point = point;
        }
    }

    private Vector3 GetPoint(float t)
    {
        return (1.0f - t) * (1.0f - t) * _p0
            + 2.0f * (1.0f - t) * t * _p1
            + t * t * _p2;
    }

    public Vector4 GetPosition(float t)
    {
        Vector3 forward;
        if (t == 0.0f)
        {
            forward = _p1 - _p0;
        }
        else if (t == 1.0f)
        {
            forward = _p2 - _p1;
        }
        else
        {
            Vector3 prevpoint = GetPoint(t - 0.1f);
            Vector3 nextpoint = GetPoint(t + 0.1f);
            forward = nextpoint - prevpoint;

        }

        float w = Vector3.Angle(Vector3.forward, forward);
        if (forward.x < 0.0f)
        {
            w = -w;
        }

        Vector4 value = GetPoint(t);
        value.w = w;

        return value;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(_p0, _p1);
        Gizmos.DrawLine(_p1, _p2);

        Gizmos.DrawCube(_p0, Vector3.one);
        Gizmos.DrawCube(_p1, Vector3.one);
        Gizmos.DrawCube(_p2, Vector3.one);
    }
}