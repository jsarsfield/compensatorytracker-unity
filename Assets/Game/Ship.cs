﻿using UnityEngine;
using System.Collections;

public class Ship : MonoBehaviour 
{
    public Camera camera;
    public Track track;


    public float speed = 0.5f;
    public float ship_x = 0.0f;
    public float ship_y = 0.0f;
    public float rotation = 0.0f;

    private const float max_x = 2.5f;
    private const float max_y = 2.0f;


    private float real_speed;
    private float real_x;
    private float real_y;
    private float real_rotation;


	void Start() 
    {
        real_speed = speed;
        real_x = ship_x;
        real_y = ship_y;
        real_rotation = rotation;
	}
	
	void Update() 
    {
        /*
        ship_x = (Input.mousePosition.x / Screen.width) * 2f - 1f;
        ship_y = (Input.mousePosition.y / Screen.height) * 2f - 1f;

        if (Input.GetMouseButton(0))
            rotation = Mathf.Lerp(rotation, 90f, 0.1f);
        else
            rotation = Mathf.Lerp(rotation, 0f, 0.1f);
        */


        ship_x = Mathf.Clamp(ship_x, -1f, 1f);
        ship_y = Mathf.Clamp(ship_y, -1f, 1f);


        real_speed = speed;
        real_x = Mathf.Lerp(real_x, ship_x, 0.1f);
        real_y = Mathf.Lerp(real_y, ship_y, 0.1f);
        real_rotation = Mathf.Lerp(real_rotation, rotation, 0.1f);



        Vector4 track_pos = track.Move(real_speed);
        Vector3 center_pos = (Vector3)track_pos + Vector3.up * 2.0f;

        transform.position = center_pos + (Quaternion.AngleAxis(track_pos.w + 90f, Vector3.up) * Vector3.forward * (real_x * max_x)) + Vector3.up * (1.0f + real_y * max_y);
        transform.rotation = Quaternion.AngleAxis(track_pos.w, Vector3.up);

        camera.transform.position = center_pos + transform.forward * -20.0f + Vector3.up * 5.0f;
        camera.transform.LookAt(center_pos);
        camera.transform.position = center_pos + transform.forward * -13.0f + Vector3.up * 5.0f;


        transform.Rotate(Vector3.forward, real_rotation);
	}
}
