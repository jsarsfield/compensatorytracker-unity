﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;


public class Track : MonoBehaviour
{
    public UnityEngine.Object TrackPrefab;
    public UnityEngine.Object[] GatePrefabs = new UnityEngine.Object[1];
    public bool AllowCorners = true;


    private const int _trackbuffersize = 8;
    private Queue<TrackPiece> _track = new Queue<TrackPiece>(_trackbuffersize);


    private const float _curvetollerance = 20.0f;
    private const float _trackminlength = 200.0f;
    private const float _trackmaxlength = 400.0f;

    private float _generatorangle = 0.0f;
    private Vector3 _generatorjoinpos = Vector3.zero;
    private Vector3 _generatorcurvepos = Vector3.forward * _trackmaxlength * 0.5f;


    private float _position = 1.0f;


	void Awake() 
    {
	    for (int i = 0; i < _trackbuffersize; i++)
        {
            GameObject instance = (GameObject)Instantiate(TrackPrefab, Vector3.zero, Quaternion.identity);
            instance.transform.parent = transform;
            TrackPiece trackinstance = instance.GetComponent<TrackPiece>();
            _track.Enqueue(trackinstance);
        }
	}

    void Start()
    {
        for (int i = 0; i < _trackbuffersize; i++)
        {
            TrackPiece trackinstance = _track.Dequeue();
            GenerateMoreTrack(trackinstance);
            _track.Enqueue(trackinstance);
        }
    }

    void Update()
    {
	
	}

    public Vector4 Move(float amount)
    {
        /*
        _position = _position + amount;

        while (_position > 2.0f)
        {
            _position -= 1.0f;

            TrackPiece moved_piece = _track.Dequeue();
            GenerateMoreTrack(moved_piece);
            _track.Enqueue(moved_piece);
        }

        return _track.ToArray()[1].GetPosition(_position - 1.0f);
        */


        while (amount > 0.0f)
        {
            float track_length = _track.ToArray()[1].length;    // aprox track length in units
            float ratio = amount / track_length;                // coneicient of the track to move for this distance


            // if ratio would put us on the next track we need to handle this
            float dist_left = (2.0f - _position) * _track.ToArray()[1].length;

            if (dist_left < amount)
            {
                amount -= dist_left;
                _position = 1.0f;

                TrackPiece moved_piece = _track.Dequeue();
                GenerateMoreTrack(moved_piece);
                _track.Enqueue(moved_piece);
            }
            else
            {
                _position += ratio;
                amount = 0.0f;
            }
        }

        return _track.ToArray()[1].GetPosition(_position - 1.0f);
    }

    void GenerateMoreTrack(TrackPiece piece)
    {
        Vector3 p0 = _generatorjoinpos;
        Vector3 p1 = _generatorcurvepos;


        float angle = 0.0f;
        if (AllowCorners)
        {
            do
            {
                angle = UnityEngine.Random.Range(-45.0f, 45.0f);
                if (angle > -_curvetollerance && angle < _curvetollerance)
                {
                    angle = 0.0f;
                }
            }
            while ((_generatorangle + angle) < -90.0f || (_generatorangle + angle) > 90.0f);
            _generatorangle += angle;
        }

        float piecelength = UnityEngine.Random.Range(_trackminlength, _trackmaxlength);

        Vector3 direction = Quaternion.AngleAxis(angle, Vector3.up) * Vector3.forward;
        direction.Normalize();

        _generatorjoinpos = _generatorcurvepos + direction * piecelength * 0.5f;
        _generatorcurvepos = _generatorcurvepos + direction * piecelength;


        Vector3 p2 = _generatorjoinpos;
        piece.GenerateTrack(p0, p1, p2);




        for (int i = 0; i < 3; i++)
        {
            Vector4 p = piece.GetPosition((float)i / 3.0f + 0.2f);
            Vector3 gate_pos = (Vector3)p;
            Quaternion gate_rot = Quaternion.AngleAxis(p.w, Vector3.up);

            int index = UnityEngine.Random.Range(0, GatePrefabs.Length);
            Instantiate(GatePrefabs[index], gate_pos, gate_rot);
        }

    }
}
